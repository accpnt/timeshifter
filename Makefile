# Make the build silent by default
V =

ifeq ($(strip $(V)),)
        E = @echo
        Q = @
else
        E = @\#
        Q =
endif
export E Q


SRC_DIR_MAIN = src
SRC_DIR_LAYER = $(SRC_DIR_MAIN)/layer/
SRC_DIR_BULLETML = $(SRC_DIR_MAIN)/libbulletml/


TARGET := tns
CSRC   := $(wildcard src/*.c) \
         $(wildcard src/layer/*.c) 
CCSRC  := $(wildcard src/libbulletml/src/*.cpp)
OBJS   := $(CSRC:.c=.o) $(CCSRC:.cpp=.o)

CC       = g++
WARNINGS = -Wall -pedantic
DEBUG    = -g
CFLAGS   = -O2 $(WARNINGS) $(DEBUG) -W -Wpointer-arith `sdl-config --cflags`
INCLUDES = -I $(SRC_DIR_MAIN) -I $(SRC_DIR_LAYER) -I $(SRC_DIR_BULLETML)/include
LFLAGS   = 
LIBS     = `sdl-config --static-libs` -lSDL_mixer -lstdc++

.PHONY: all clean distclean 
all:: ${TARGET} ${PLUGIN}

${TARGET}: ${OBJS} 
	$(E) "  LINK    " $@
	$(Q) ${CC} ${LFLAGS} -o $@ $^ ${LIBS} 

${OBJS}: %.o: ${CSRC} ${CCSRC}    
	$(E) "  CC      " $@
	$(Q) ${CC} ${CFLAGS} ${INCLUDES} -o $@ -c $< 

clean:: 
	$(E) "  CLEAN"
	$(Q) -rm -f *~ ${OBJS} ${TARGET} ${PLUGIN}

distclean:: clean

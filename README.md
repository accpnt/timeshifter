# Toki no shūgeki

## About

"Toki no shūgeki" (Time Assault) is a shooting-game forked from [noiz2sa](http://www.asahi-net.or.jp/~cs8k-cyu/windows/noiz2sa_e.html) by Kenta Cho. 

Concept : avoir une jauge de temps qui permettrait au joueur de ralentir ou d’accélérer le déplacement des bullets à sa guise. Si le joueur se sent en difficulté, il peut utiliser sa jauge pour décélérer le déplacement des bullets. A l’inverse, s’il est en confiance, il peut remplir de nouveau sa jauge de temps en accélérant la vitesse de déplacement des bullets.

Style graphique : REZ, Tron

## Todo

* Code cleanup
* Find Makefile templates
* Wrapper for SDL specific calls
* Add time gauge
* Add boss gauge
* Think about a way to draw real ships
* Add Tate + Yoko modes (check how Ikaruga handles them)
* Understand letterdata.h and clrtbl.c
* Use new libbulletml
* Machine Learning algorithm ? 

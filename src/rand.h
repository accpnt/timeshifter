/*
 * $Id: rand.h,v 1.1.1.1 2002/11/03 11:08:24 kenta Exp $
 *
 * Copyright 2002 Kenta Cho. All rights reserved.
 */

/**
 * Make random number function.
 *
 * @version $Revision: 1.1.1.1 $
 */
#ifndef __RAND_H__
#define __RAND_H__

unsigned int nextRandInt(unsigned int *v);

#endif  /* __RAND_H__ */

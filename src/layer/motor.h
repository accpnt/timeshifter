#ifndef __MOTOR_H__
#define __MOTOR_H__

#include "tns.h"

extern Status status;

Uint32 layer_get_ticks();
void layer_quit();
void layer_delay(Uint32 delay);
int layer_handle_event();

#endif  /* __MOTOR_H__ */

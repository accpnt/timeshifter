#include "SDL.h"
#include "motor.h"
#include "screen.h"

/* globals */
SDL_Event event;

static int pPrsd = 1;

Uint32 layer_get_ticks() {
	return SDL_GetTicks();
}

void layer_quit() {
	SDL_Quit();
}

void layer_delay(Uint32 delay) {
	SDL_Delay(delay);
}

int layer_handle_event() {
	int done = 0;

	SDL_PollEvent(&event);
	keys = SDL_GetKeyState(NULL);

	if (keys[SDLK_ESCAPE] == SDL_PRESSED || event.type == SDL_QUIT)
		done = 1;
	if (keys[SDLK_p] == SDL_PRESSED) {
		if (!pPrsd) {
			if (status == IN_GAME) {
				status = PAUSE;
			} else if (status == PAUSE) {
				status = IN_GAME;
			}
		}
		pPrsd = 1;
	} else {
		pPrsd = 0;
	}

	return done;
}

/*
 * $Id: tns.c,v 1.8 2003/02/12 13:55:13 kenta Exp $
 *
 * Copyright 2002 Kenta Cho. All rights reserved.
 */

/**
 * Noiz2sa main routine.
 *
 * @version $Revision: 1.8 $
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attractmanager.h"
#include "background.h"
#include "bonus.h"
#include "brgmng_mtd.h"
#include "degutil.h"
#include "foe_mtd.h"
#include "frag.h"
#include "tns.h"
#include "screen.h"
#include "ship.h"
#include "shot.h"
#include "soundmanager.h"
#include "vector.h"
#include "motor.h"

/* globals */
Config cfg = {
	.no_sound = 0,
	.accframe = 0
};

Status status;

static float stagePrm[STAGE_NUM + ENDLESS_STAGE_NUM + 1][3] = {
		{13, 0.5f, 0.12f}, {2, 1.8f, 0.15f},  {3, 3.2f, 0.1f},  {90, 6.0f, 0.3f},
		{5, 5.0f, 0.6f},   {6, 10.0f, 0.6f},  {7, 5.0f, 2.2f},  {98, 12.0f, 1.5f},
		{9, 10.0f, 2.0f},  {79, 21.0f, 1.5f}, {-3, 5.0f, 0.7f}, {-1, 10.0f, 1.2f},
		{-4, 15.0f, 1.8f}, {-2, 16.0f, 1.8f}, {0, -1.0f, 0.0f},
};

// Initialize and load preference.
static void initFirst() {
	loadPreference();
	srand(layer_get_ticks());
	initBarragemanager();
	initAttractManager();
}

// Quit and save preference.
void quitLast() {
	if (!cfg.no_sound)
		closeSound();
	savePreference();
	closeBarragemanager();
	layer_close();
	layer_quit();
	exit(1);
}

void initTitleStage(int stg) {
	initFoes();
	initBarrages(stagePrm[stg][0], stagePrm[stg][1], stagePrm[stg][2]);
}

void initTitle() {
	int stg;
	status = TITLE;

	stg = initTitleAtr();
	initShip();
	initShots();
	initFrags();
	initBonuses();
	initBackground();
	setStageBackground(1);

	initTitleStage(stg);
}

void initGame(int stg) {
	status = IN_GAME;

	initShip();
	initShots();
	initFoes();
	initFrags();
	initBonuses();
	initBackground();

	initBarrages(stagePrm[stg][0], stagePrm[stg][1], stagePrm[stg][2]);
	initGameState(stg);
	if (stg < STAGE_NUM) {
		setStageBackground(stg % 5 + 1);
		playMusic(stg % 5 + 1);
	} else {
		if (!insane) {
			setStageBackground(0);
			playMusic(0);
		} else {
			setStageBackground(6);
			playMusic(6);
		}
	}
}

void initGameover() {
	status = GAMEOVER;
	initGameoverAtr();
}

void initStageClear() {
	status = STAGE_CLEAR;
	initStageClearAtr();
}

static void move() {
	switch(status) {
	case TITLE:
		moveTitleMenu();
		moveBackground();
		addBullets();
		moveFoes();
		break;
	case IN_GAME:
		moveBackground();
		addBullets();
		moveShots();
		moveShip();
		moveFoes();
		moveFrags();
		moveBonuses();
		break;
	case GAMEOVER:
		moveGameover();
		moveBackground();
		addBullets();
		moveShots();
		moveFoes();
		moveFrags();
		break;
	case STAGE_CLEAR:
		moveStageClear();
		moveBackground();
		moveShots();
		moveShip();
		moveFrags();
		moveBonuses();
		break;
	case PAUSE:
		movePause();
		break;
	}
}

static void draw() {
	switch (status) {
	case TITLE:
		// Draw background.
		drawBackground();
		drawFoes();
		drawBulletsWake();
		blendScreen();
		// Draw foreground.
		drawBullets();
		drawScore();
		drawTitleMenu();
		break;
	case IN_GAME:
		// Draw background.
		drawBackground();
		drawBonuses();
		drawFoes();
		drawBulletsWake();
		drawFrags();
		blendScreen();
		// Draw foreground.
		drawShots();
		drawShip();
		drawBullets();
		drawScore();
		break;
	case GAMEOVER:
		// Draw background.
		drawBackground();
		drawFoes();
		drawBulletsWake();
		drawFrags();
		blendScreen();
		// Draw foreground.
		drawShots();
		drawBullets();
		drawScore();
		drawGameover();
		break;
	case STAGE_CLEAR:
		// Draw background.
		drawBackground();
		drawBonuses();
		drawFrags();
		blendScreen();
		// Draw foreground.
		drawShots();
		drawShip();
		drawScore();
		drawStageClear();
		break;
	case PAUSE:
		// Draw background.
		drawBackground();
		drawBonuses();
		drawFoes();
		drawBulletsWake();
		drawFrags();
		blendScreen();
		// Draw foreground.
		drawShots();
		drawShip();
		drawBullets();
		drawScore();
		drawPause();
		break;
	}
}

static void usage(char *argv0) {
	fprintf(stderr,
					"Usage: %s [-nosound] [-reverse] [-brightness n] [-nowait] "
					"[-accframe]\n",
					argv0);
}

static void parse_args(int argc, char * argv[]) {
	int i;
	for (i = 1; i < argc; i++) {
		if (strcmp(argv[i], "-nosound") == 0) {
			cfg.no_sound = 1;
		} else if (strcmp(argv[i], "-reverse") == 0) {
			cfg.button_reversed = 1;
		} else if ((strcmp(argv[i], "-brightness") == 0) && argv[i + 1]) {
			i++;
			cfg.brightness = (int)atoi(argv[i]);
			if ((cfg.brightness < 0) || (256 < cfg.brightness)) {
				cfg.brightness = DEFAULT_BRIGHTNESS;
			}
		} else if (strcmp(argv[i], "-nowait") == 0) {
			cfg.no_wait = 1;
		} else if (strcmp(argv[i], "-accframe") == 0) {
			cfg.accframe = 1;
		} else {
			usage(argv[0]);
			exit(1);
		}
	}
}

int interval = INTERVAL_BASE;
int tick = 0;

int main(int argc, char *argv[]) {
	int done = 0;
	long prvTickCount = 0;
	int i;
	int btn;
	long nowTick;
	int frame;

	parse_args(argc, argv);

	initDegutil();
	layer_init();
	if (!cfg.no_sound)
		initSound();
	initFirst();
	initTitle();

	while (!done) {
		done = layer_handle_event();

		nowTick = layer_get_ticks();
		frame = (int)(nowTick - prvTickCount) / interval;
		if (frame <= 0) {
			frame = 1;
			layer_delay(prvTickCount + interval - nowTick);
			if (cfg.accframe) {
				prvTickCount = layer_get_ticks();
			} 
			else {
				prvTickCount += interval;
			}
		} 
		else if (5 < frame) {
			frame = 5;
			prvTickCount = nowTick;
		} 
		else {
			prvTickCount += frame * interval;
		}

		for (i = 0; i < frame; i++) {
			move();
			tick++;
		}

		smokeScreen();
		draw();
		flipScreen();
	}

	quitLast();
}

/*
 * $Id: tns.h,v 1.4 2003/02/09 07:34:16 kenta Exp $
 *
 * Copyright 2002 Kenta Cho. All rights reserved.
 */
#ifndef __TNS_H__
#define __TNS_H__

#define randN(N) (rand() % (N))
#define randNS(N) (rand() % (N << 1) - N)
#define randNS2(N) ((rand() % (N) - (N >> 1)) + (rand() % (N) - (N >> 1)))
#define absN(a) ((a) < 0 ? -(a) : (a))

#define INTERVAL_BASE 16

#define CAPTION "Noiz2sa"
#define VERSION_NUM 50

#define NOT_EXIST -999999

typedef enum { 
	TITLE = 0, 
	IN_GAME,
	GAMEOVER,
	STAGE_CLEAR,
	PAUSE
} Status;

typedef enum {
	TATE = 0,
	YOKO
} Mode;

typedef struct {
	int  no_sound;
	int  button_reversed;
	int  brightness;
	int  no_wait;
	int  accframe;
	Mode mode;
} Config;

extern Status status;
extern int interval;
extern int tick;
extern Config cfg;

void quitLast();
void initTitleStage(int stg);
void initTitle();
void initGame(int stg);
void initGameover();
void initStageClear();

#endif  /* __TNS_H__ */
